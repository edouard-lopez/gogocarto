<?php

namespace App\Services;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Doctrine\ODM\MongoDB\DocumentManager;

class TaxonomyService
{
    protected $serializer;
    protected $dm;

    public function __construct(SerializerInterface $serializer, DocumentManager $dm)
    {
        $this->serializer = $serializer;
        $this->dm = $dm;
    }

    public function getTaxonomyJson()
    {
        $cache = new FilesystemAdapter();
        return $cache->get($this->cacheKey('taxonomy'), function () {
            return $this->generateTaxonomyJson();
        });
    }

    public function getOptionsJson()
    {
        $cache = new FilesystemAdapter();
        return $cache->get($this->cacheKey('options'), function () {
            return $this->generateOptionsJson();
        });
    }

    private function cacheKey($suffix) {
        $lastOption = $this->dm->query('Option')->sort('updatedAt', 'desc')->getOne();
        $lastOptionKey = $lastOption ? $lastOption->getUpdatedAt()->format('U') : 'none';
        $lastCategory = $this->dm->query('Category')->sort('updatedAt', 'desc')->getOne();
        $lastCategoryKey = $lastCategory ? $lastCategory->getUpdatedAt()->format('U') : 'none';
        $dbName = $this->dm->getConfiguration()->getDefaultDB();
        return "taxonomy_cache_{$dbName}_{$lastOptionKey}_{$lastCategoryKey}_$suffix";
    }

    // Create flatten option list
    private function generateOptionsJson()
    {
        $options = $this->dm->get('Option')->findAll();

        $optionsSerialized = [];
        foreach ($options as $option) {
            try {
                $optionsSerialized[] = $this->serializer->serialize($option, 'json', SerializationContext::create()->setGroups(['semantic']));
            } catch (\Exception $e) {}
        }
        return '['.implode(', ', $optionsSerialized).']';
    }

    // Create hierachic taxonomy
    private function generateTaxonomyJson()
    {
        $rootCategories = $this->dm->get('Category')->findRootCategories();

        if (0 == count($rootCategories)) {
            return '[]';
        } else {
            $rootCategoriesSerialized = [];
            foreach ($rootCategories as $key => $rootCategory) {
                $rootCategoriesSerialized[] = $this->serializer->serialize($rootCategory, 'json');
            }
            return '['.implode(', ', $rootCategoriesSerialized).']';
        }
    }
}
